#pragma once
#include "stdafx.h"

void InitDSound();
void InitChaoSounds();
void InitChaoBGSounds();
void InitDelayedSounds();
void InitFadeout();
void InitSoundHooks();
void SoundFadeExec();
void DelayedSoundsExec();

SEDistanceOverride DistanceOverrideTable[] = {
    { SE_SHT_FLY_ENEMY, 1000.0f, 2000.0f, 0 },
    { SE_SD_CANB_FIRE, 50.0f, 500.0f, 0 },
    { SE_M2_B_SHOT, 20.0f, 500.0f, 0 },
    { SE_M2_JUMP, 20.0f, 500.0f, 0 },
    { SE_M1_JET, 20.0f, 500.0f, 0 },
    { SE_M2_B_BOMB, 20.0f, 500.0f, 0 },
    { SE_CS_SWIM, 20.0f, 500.0f, 0 },
    { SE_CS_INWATER, 80.0f, 500.0f, 0 },
    { SE_SD_CANH_FIRE, 100.0f, 1000.0f, 96 },
    { SE_SD_CANH_FIRE, 100.0f, 1000.0f, 75 },
    { SE_SD_CANH_ROT, 100.0f, 1000.0f, 0 },
    { SE_M3_ENERGYSHOT, 50.0f, 500.0f, 0 },
    { SE_M3_NEEDLEROT, 50.0f, 500.0f, 0 },
};
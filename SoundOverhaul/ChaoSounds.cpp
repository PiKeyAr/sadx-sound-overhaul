#include "stdafx.h"
#include "ChaoSounds.h"

// Chao sound hooks

bool EnableDCChaoVoices = false; // Not implemented

void PlayChaoRaceCheer(MusicIDs id)
{
	if (CurrentSong != id)
	{
		if (Music_Enabled)
		{
			CurrentSong = id;
			LastSong2 = id;
			LastSong = id;
		}
	}
	PlaySound_Timer(SE_CH_HOGE5, NULL, 8, 100, 1000);
}

static Trampoline* ECGarden_Main_t = nullptr;
static void __cdecl ECGarden_Main_r(ObjectMaster* a1)
{
	auto original = TARGET_DYNAMIC(ECGarden_Main);
	PlaySound_Timer(SE_CH_HOGE7, a1->Data1, 8, 100, 100); // Beach
	a1->Data1->Position = { -33.75f, 59.625f, 7.875f };
	PlaySound_Positional_Timer(SE_CH_HOGE6, a1->Data1, 0, 0, 100, -33.75f, 59.625f, 7.875f); // Radar
	original(a1);
}

static Trampoline* MRGarden_Main_t = nullptr;
static void __cdecl MRGarden_Main_r(ObjectMaster* a1)
{
	auto original = TARGET_DYNAMIC(MRGarden_Main);
	PlaySound_Timer(SE_CH_BEACH, a1->Data1, 8, 100, 100); //Mushi
	a1->Data1->Position = { 31.5f, 18.0f, -101.25f };
	PlaySound_Positional_Timer(SE_CH_SPRING, a1->Data1, 0, 0, 100, 31.5f, 18.0f, -101.25f);
	original(a1);
}

void InitChaoSounds()
{
	ChaoSoundbanks[1].Filename = "ALIFE_BANK01";
	if (EnableDCChaoVoices) // Not implemented
	{
		ChaoSoundbanks[3].Filename = "ALIFE_BANK05"; //the first voice bank (bank 4 in DX) is replaced with background sounds (bank 5 on DC)
		ChaoSoundbanks[4].Filename = "ALIFE_BANK04"; //the second voice bank (bank 5 in DX) is replaced with SA1 voices (bank 4 on DC)
		ChaoSoundbanks2[2].Filename = "ALIFE_BANK05";
		ChaoSoundbanks2[3].Filename = "ALIFE_BANK04";
	}
	SoundLists[61].List = ChaoSoundbanks;
	SoundLists[61].Count = 6;
	SoundLists[2].List = ChaoSoundbanks2; // Might be unused
	SoundLists[2].Count = 5;
	for (int i = 0; i < LengthOfArray(ChaoSoundReplacements); i++)
	{
		WriteData((__int16*)(ChaoSoundReplacements[i].address + 1), ChaoSoundReplacements[i].id);
	}
	WriteData((__int16*)0x0072FDBA, (__int16)SE_AL_TAIKO);
	WriteData((__int16*)0x0072FDC3, (__int16)SE_AL_RAPPA);
	WriteData((__int16*)0x0072FDCC, (__int16)SE_AL_FUE);
	WriteData((__int16*)0x0072FDD5, (__int16)SE_AL_THINBAL);
	WriteData((__int16*)0x0072FDDE, (__int16)SE_AL_AGOGOBELL);
	WriteData((__int16*)0x0072FDE7, (__int16)SE_AL_TAMBOURINE);
	WriteData((__int16*)0x0072FDF0, (__int16)SE_AL_BONGO);
	WriteData((__int16*)0x0072FDF9, (__int16)SE_AL_SUZU);
	WriteData<1>((Uint8*)0x0044262F, 0x36u); // Gamma whistle sound is taken from the Chao SFX soundbank
	WriteCall((void*)0x0072E618, PlayChaoRaceCheer);
}

void InitChaoBGSounds()
{
	ECGarden_Main_t = new Trampoline(0x718FD0, 0x718FD5, ECGarden_Main_r);
	MRGarden_Main_t = new Trampoline(0x718C30, 0x718C35, MRGarden_Main_r);
}
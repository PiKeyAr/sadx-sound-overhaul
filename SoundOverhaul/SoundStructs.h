#pragma once
#include "stdafx.h"

// SADX structs and enums

enum SoundFlags
{
	SoundFlags_NoCheckRepeat = 0x1, //Add sound to the queue even if the same sound ID is playing
	SoundFlags_VolumeDist = 0x100, //Enable distance-based volume control (XYZ)
	SoundFlags_VolumeManual = 0x200, //Enable manual volume control
	SoundFlags_Pan = 0x800, //Enable pan control
	SoundFlags_Pitch = 0x2000, //Enable pitch control
	SoundFlags_No3D = 0x20, //Set for 3D sounds when 3D sound is disabled in config
	SoundFlags_3D = 0x4000, //Dolby/HRTF stereo panning
    SoundFlags_QSound = 0x1000, //Dreamcast leftover, doesn't seem to do anything in SADX
	SoundFlags_AutoStop = 0x10, //Stop the sound when the timer runs out
	SoundFlags_Unknown_A = 0x2, //Used internally for something
	SoundFlags_Fadeout = 0x4, //I added this for custom use, SADX doesn't have it
};

struct SoundEntry
{
	int pri; //-1 for top priority
	int timer; //-1 to loop indefinitely, stops at 0
	EntityData1* entity; //Sound stops when entity is destroyed, can be NULL
	int flags; //SoundFlags
	int id; //SoundIDs
	int pan;
	int volume;
	int volume_max; //Also stores an entity pointer for 3D sounds
	int pitch;
	NJS_VECTOR pos;
	int qnum; //Set to -1 when an entity isn't already in the 3D sound queue
	int banknum; //Unused
};
// stdafx.h" : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#define TARGET_DYNAMIC(name) ((decltype(name##_r)*)name##_t->Target())

#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <string>
#include "SADXModLoader.h"
#include "IniFile.hpp"
#include "Trampoline.h"
#include "ModelInfo.h"
#include "LandTableInfo.h"
#include "SoundStructs.h"
#include "SoundStructsCustom.h"
#include "DataPointers.h"
#include "stdafx.h"
#include "DelayedSounds.h"

int DelayedSoundCount = 0;

void PlaySound_Delayed(int sound_id, EntityData1* a2, int min_index, int volume, int timer, EntityData1* a6)
{
	for (int i = 0; i < LengthOfArray(DelayedSounds); i++)
	{
		if (DelayedSounds[i].sound_id == -1)
		{
			DelayedSounds[i].sound_id = sound_id;
			DelayedSounds[i].entity1 = a2;
			DelayedSounds[i].min_index = min_index;
			DelayedSounds[i].volume = volume;
			DelayedSounds[i].timer = timer;
			DelayedSounds[i].is3d = true;
			DelayedSounds[i].volume_max = (int)a6;
			DelayedSoundCount++;
			return;
		}
	}
	PlaySound_3D_Timer_pri(sound_id, a2, min_index, volume, timer, a6);
}

int PlaySound_StandardTimer_Delay(SoundIDs ID, void* entity, int pri, int volume)
{
	for (int i = 0; i < 20; i++)
	{
		if (DelayedSounds[i].sound_id == -1)
		{
			DelayedSounds[i].sound_id = ID;
			DelayedSounds[i].entity1 = (EntityData1*)entity;
			DelayedSounds[i].min_index = pri;
			DelayedSounds[i].volume = volume;
			DelayedSounds[i].is3d = false;
			DelayedSoundCount++;
			return 1;
		}
	}
	return PlaySound_StandardTimer(ID, entity, pri, volume);
}

static Trampoline* ORRaf_Pole_Main_t = nullptr;
static void __cdecl ORRaf_Pole_Main_r(ObjectMaster* a1)
{
	auto original = TARGET_DYNAMIC(ORRaf_Pole_Main);
	if (a1->Data1->Scale.y >= 0.35f && a1->Data1->Scale.y < 0.36f)
		PlaySound_Delayed(SE_LW_POLE, a1->Data1, -1, 0, 120, a1->Data1);
	original(a1);
}

void PlaySound_Delayed_Rock(int sound_id, EntityData1* a2, int min_index, int volume, int timer, EntityData1* a6)
{
	PlaySound_Delayed(sound_id, a2, min_index, volume, timer + 120, a6);
}

void SwingingSpikeBallFix(SoundIDs a1, EntityData1* entity, int pri, int volume, int playlength, EntityData1* entity_origin)
{
	int v6 = SoundQueue_GetOtherThing(a1, entity);
	if (v6 == -1) 
		PlaySound_Delayed(a1, entity, pri, volume, 130, entity_origin);
}

void DelayedSoundsExec()
{
	bool DelayedSoundPlayed = false;
	if (DelayedSoundCount)
	{
		for (int i = 0; i < 20; i++)
		{
			if (!DelayedSoundPlayed && DelayedSounds[i].sound_id != -1)
			{
				if (DelayedSounds[i].is3d)
				{
					if (DelayedSounds[i].sound_id == SE_C_IRONSPIN)
						PlaySound_3D_Timer(DelayedSounds[i].sound_id, DelayedSounds[i].entity1, DelayedSounds[i].min_index, DelayedSounds[i].volume, DelayedSounds[i].timer, (EntityData1*)DelayedSounds[i].volume_max);
					else
						PlaySound_3D_Timer_pri(DelayedSounds[i].sound_id, DelayedSounds[i].entity1, DelayedSounds[i].min_index, DelayedSounds[i].volume, DelayedSounds[i].timer, (EntityData1*)DelayedSounds[i].volume_max);
				}
				else
					PlaySound_StandardTimer(DelayedSounds[i].sound_id, DelayedSounds[i].entity1, DelayedSounds[i].min_index, DelayedSounds[i].volume);
				DelayedSoundCount--;
				DelayedSounds[i].sound_id = -1;
				DelayedSoundPlayed = true;
			}
		}
	}
}

void InitDelayedSounds()
{
	for (int i = 0; i < 20; i++)
	{
		DelayedSounds[i].sound_id = -1;
		DelayedSounds[i].timer = -1;
	}
	ORRaf_Pole_Main_t = new Trampoline(0x5EC280, 0x5EC286, ORRaf_Pole_Main_r);
	WriteCall((void*)0x604189, PlaySound_Delayed_Rock); // Red Mountain rocks
	WriteCall((void*)0x7A3B22, PlaySound_Delayed); // OFeBjg
	WriteCall((void*)0x47AE23, PlaySound_StandardTimer_Delay); // Knuckles' Maximum Heat release
	WriteCall((void*)0x4CAF88, PlaySound_StandardTimer_Delay); // Explosion in Sky Deck
	WriteCall((void*)0x5B57D8, PlaySound_Delayed); // Final Egg targets
	WriteCall((void*)0x7A3616, SwingingSpikeBallFix); // Swinging spike balls
	WriteCall((void*)0x62E821, PlaySound_StandardTimer_Delay); // Sky Chase final boss missiles
}
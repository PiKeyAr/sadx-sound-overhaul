#pragma once
#include "stdafx.h"

FunctionPointer(void, ORRaf_Pole_Main, (ObjectMaster* a1), 0x05EC280);

struct MiniSoundQueue
{
	int sound_id;
	EntityData1* entity1;
	int min_index;
	int volume;
	int timer;
	int volume_max;
	bool is3d;
};

MiniSoundQueue DelayedSounds[20];
#pragma once
#include "stdafx.h"

struct SEFadeoutData
{
	int ID;
	int start;
	int speed;
};

SEFadeoutData FadeoutTable[] = {
	{ SE_SPINCHARGE, 110, 3 },
	{ SE_WV_WIND, 30, 4 },
	{ SE_FE_FAN, 30, 4 },
	//{ SE_PC_SS_ONWATER, 30, 3},
	//{ SE_PC_SS_ONWATER2, 30, 3},
	//{ SE_PCB_SONICUP, 80, 4 },
};
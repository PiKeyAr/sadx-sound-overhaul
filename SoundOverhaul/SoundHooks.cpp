#include "stdafx.h"

// Various WriteCall hooks
 
// Loads the Sonic voice bank so that Sonic can say "Whoah!" when Knuckles attacks him
void WhoahSomethingBuggingYou()
{
	LoadSoundList(VoiceLanguage ? 70 : 69);
	LoadSoundList(VoiceLanguage ? 72 : 71);
}

// Plays the character damage grunt
void PlayCharacterHurtVoice(int ID, void* a2, int a3, int a4)
{
	PlaySound_StandardTimer(ID, a2, a3, a4);
	int voice;
	switch (GetCurrentCharacterID())
	{
	case Characters_Sonic:
	case Characters_MetalSonic:
	default:
		voice = SE_DAMAGE2;
		break;
	case Characters_Tails:
		voice = SE_MV_DAMAGE;
		break;
	case Characters_Knuckles:
		voice = SE_KV_DAMAGE;
		break;
	case Characters_Amy:
		voice = SE_AV_DAMAGE;
		break;
	case Characters_Big:
		voice = SE_BV_DAMAGE;
		break;
	case Characters_Gamma:
		voice = SE_EV_DAMAGE;
		break;
	}
	PlaySound_StandardTimer(voice, 0, 0, 0);
	return;
}

void DolphinSoundFix(int a1, EntityData1* a2, int a3, int a4, int a5, EntityData1* a6)
{
	PlaySound_3D_Timer_pri(a1, a2, a3, a4, 180 * ((FramerateSetting >= 2) ? 2 : 1), a6);
}

void EggWalkerMissileFix(int sound_id, EntityData1* a2, int a3, int a4, int a5, EntityData1* a6)
{
	PlaySound_3D_Timer_pri(sound_id, a2, a3, a4, a5, EntityData1Ptrs[0]);
}

void ZeroNameSoundFix(SoundIDs ID, void* entity, int pri, int volume)
{
	PlaySound_StandardTimer(CurrentLevel == LevelIDs_Zero ? SE_AZ_TELOP : ID, entity, pri, volume);
}

void SpaceshipFix(SoundIDs a1, EntityData1* entity, int pri, int volume, int playlength, EntityData1* entity_origin)
{
	PlaySound_Positional_Timer(a1, entity, pri, 100, 120, entity->Position.x, entity->Position.y, entity->Position.z);
}

void DescendingIntoTheClouds(SoundIDs ID, void* entity, int pri, int volume)
{
	PlaySound_Timer(VoiceLanguage ? ID : ID - 2, entity, 2, volume, 240);
}

int KnuxGlide(SoundIDs ID, void* entity, int pri, int volume)
{
	return PlaySound_Timer(ID, entity, pri, volume, 120);
}

int PirateShipFix(SoundIDs id, EntityData1* entity, int pri, int volume, int playlength, float x, float y, float z)
{
	PlaySound_Positional_Timer(id, entity, pri, volume, playlength, entity->Position.x, entity->Position.y + 350.0f, entity->Position.z);
	return 1;
}

void CannonFix(SoundIDs a1, EntityData1* entity, int pri, int volume, int playlength, EntityData1* entity_origin)
{
	//PlaySound_3D_Timer(a1, EntityData1Ptrs[0], pri, volume, playlength, entity_origin);
	PlaySound_Positional_Timer(a1, entity_origin, pri, 105, playlength, entity_origin->Position.x, entity_origin->Position.y, entity_origin->Position.z);
}

void WormFix(SoundIDs id, EntityData1* entity, int index, int volume, int length, float multiplier, EntityData1* origin_entity)
{
	PlaySound_Positional_Timer(id, entity, index, volume, length, origin_entity->Position.x, origin_entity->Position.y, origin_entity->Position.z);
}

int PerfectChaosRoar(SoundIDs ID, void* entity, int pri, int volume)
{
	return PlaySound_Timer(ID, NULL, 0, volume, 200);
}

void GrassFix(SoundIDs sound_id, EntityData1* entity, int pri, int volume, float x, float y, float z)
{
	PlaySound_Positional_Timer(sound_id, entity, pri, volume, 120, x, y, z);
}

void TailsWhatAmIGonnaDoWithYou()
{
	Cutscene_PlaySoundWithPitch(0, 1335, 1800);
}

void UhhhhaaaAughh(int a1)
{
	if (VoiceLanguage)
		Cutscene_Voice(472);
	Cutscene_WaitForInput(a1);
}

void UhhhhaaaAughh2(int a1)
{
	if (!VoiceLanguage)
		Cutscene_Voice(472);
}

int Pitch1(int queue_id, int pitch, int length)
{
	return Cutscene_SetSoundPitch(queue_id, 30000, length);
}

int Pitch2(int queue_id, int pitch, int length)
{
	return Cutscene_SetSoundPitch(queue_id, -25000, length);
}

signed int PerfectChaosFix(SoundIDs sound_id, EntityData1* entity, int pri, int volume)
{
	return PlaySound_Timer(sound_id, entity, pri, volume, 600);
}

void GammaTargetPositional(SoundIDs ID, EntityData1* entity, int pri, int volume)
{
	PlaySound_Positional_StandardTimer(ID, entity, pri, -50, EntityData1Ptrs[0]->Position.x, EntityData1Ptrs[0]->Position.y, EntityData1Ptrs[0]->Position.z);
}

void InitSoundHooks()
{
	WriteCall((void*)0x561A36, PerfectChaosRoar);
	WriteCall((void*)0x561FE7, PerfectChaosRoar);
	WriteCall((void*)0x562067, PerfectChaosRoar);
	WriteCall((void*)0x538304, GrassFix); // Rustling grass
	WriteCall((void*)0x478AEB, KnuxGlide); // Knuckles' glide proper loop
	WriteCall((void*)0x4C4E5B, GammaTargetPositional); // Make Gamma's target sound positional
	WriteCall((void*)0x5ECB4F, DescendingIntoTheClouds); // Descending into the clouds and Go up! Full speed ahead!
	WriteCall((void*)0x4FADCF, DolphinSoundFix); // Framerate
	WriteCall((void*)0x561E28, PerfectChaosFix); // Emerging
	WriteCall((void*)0x561F7D, PerfectChaosFix); // Sinking
	// Volume fixes
	WriteCall((void*)0x57B00C, EggWalkerMissileFix); // Missing entity fix
	WriteCall((void*)0x79B8A0, SpaceshipFix); // Twinkle Park spaceship too quiet
	WriteCall((void*)0x5F0236, CannonFix); // Sky Deck Act 2 cannon background noise
	WriteCall((void*)0x620FA0, PirateShipFix); // Pirate ships in Twinkle Park
	WriteCall((void*)0x599A7B, WormFix); // Sand Hill worm
	// Missing/wrong sounds
	WriteCall((void*)0x4507FA, PlayCharacterHurtVoice); // Play hurt voices
	WriteCall((void*)0x4B34E4, ZeroNameSoundFix);
	// Cutscene fixes
	WriteCall((void*)0x6CE25C, WhoahSomethingBuggingYou); // Load player soundbanks to play the "whoah!" voice when Knuckles attacks Sonic/Tails
	WriteCall((void*)0x6E96B9, TailsWhatAmIGonnaDoWithYou); // Tails, what am I gonna do with you?
	// Pitch fixes
	WriteCall((void*)0x6DEC13, Pitch1); // Sonic after Sky Chase
	WriteCall((void*)0x6DEC39, Pitch2); // Sonic after Sky Chase
	WriteCall((void*)0x6DED88, Pitch1); // Sonic after Sky Chase
	WriteCall((void*)0x6DED9D, Pitch2); // Sonic after Sky Chase
	WriteCall((void*)0x69B3BD, Pitch1); // Amy Final Egg
	WriteCall((void*)0x69B3DD, Pitch2); // Amy Final Egg
	// Timing fixes for voices
	WriteCall((void*)0x6E17FD, UhhhhaaaAughh2); // Sonic bumps into Knuckles
	WriteCall((void*)0x6E10CD, UhhhhaaaAughh); // Sonic bumps into Knuckles
}
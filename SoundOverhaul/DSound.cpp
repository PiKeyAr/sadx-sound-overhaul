#include "stdafx.h"
#include <SADXModLoader.h>
#include "SoundStructs.h"
#include "DataPointers.h"

// Old mod stuff used when BASS is disabled in SADX Mod Manager

int ConvertLinearToDirectX(int value, int max);

// char __usercall@<al>(int a1@<ecx>, int id, float x, float y, float z)
static const void* const Set3DSoundVolumePtr = (void*)0x4102C0;

static inline char Set3DSoundVolume_3(int a1, int id, float x, float y, float z)
{
	char result;
	__asm
	{
		push[z]
		push[y]
		push[x]
		push[id]
		mov ecx, [a1]
		call Set3DSoundVolumePtr
		add esp, 16
		mov result, al
	}
	return result;
}

char __cdecl Set3DSoundVolume_2(int a1, int id, float x, float y, float z)
{
	//PrintDebug("ID SND X Y Z: %d, %d, %f, %f, %f \n", id, SoundQueue[id].id, x, y, z);
	return Set3DSoundVolume_3(z, id, x, y, z);
}

static void __declspec(naked) Set3DPositionPCM_Hook()
{
	__asm
	{
		push[esp + 10h] // z
		push[esp + 10h] // y
		push[esp + 10h] // x
		push[esp + 10h] // id
		push ecx // a1

		// Call your __cdecl function here:
		call Set3DSoundVolume_2

		pop ecx // a1
		add esp, 4 // id
		add esp, 4 // x
		add esp, 4 // y
		add esp, 4 // z
		retn
	}
}

void __cdecl lsndVolume(int volume, int queue_id)
{
	int flags; // ecx
	int volume_conv; // esi
	flags = SoundQueue[queue_id].flags;
	if (volume > -100)
	{
		if (SoundQueue[queue_id].pri == 8) 
			volume_conv = ConvertLinearToDirectX(200, 227);
		else 
			volume_conv = ConvertLinearToDirectX(min(volume, 127) + 100, 227);//2 * (5 * volume - 640); //
	}
	else
	{
		volume_conv = -10000;
		if (flags & SoundFlags_3D)
		{
			Set3DSoundVolume_2(flags, queue_id, 0.0f, 5000.0f, 0.0f);
			Critical_SetSoundVolume(queue_id, -10000);
			return;
		}
		if (flags & SoundFlags_Fadeout)
		{
			SoundQueue[queue_id].flags &= ~SoundFlags_Fadeout;
			SoundQueue[queue_id].timer = 1;
		}
		//PrintDebug("lsndVolume: id %d, muted\n", queue_id);
	}
	//PrintDebug("lsndVolume: id %d, volume %d, dsound: %d\n", queue_id, volume, volume_conv);
	Critical_SetSoundVolume(queue_id, volume_conv);
}

void UnpauseSound_Fixed(int queue_id, int volume_unconv)
{
	int flags;
	int conv_volume;
	int volume_conv;
	// Unconvert SADX volume
	if (volume_unconv == -10000) conv_volume = -100; else conv_volume = ((volume_unconv / 2) + 640) / 5;
	flags = SoundQueue[queue_id].flags;
	if (conv_volume > -100)
	{
		if (SoundQueue[queue_id].pri == 8) volume_conv = ConvertLinearToDirectX(200, 227);
		else volume_conv = ConvertLinearToDirectX(min(conv_volume, 127) + 100, 227);//2 * (5 * volume - 640); //
	}
	else
	{
		volume_conv = -10000;
		if (flags & SoundFlags_3D)
		{
			Set3DSoundVolume_2(flags, queue_id, 0.0f, 5000.0f, 0.0f);
			Critical_SetSoundVolume(queue_id, -10000);
			//PrintDebug("Unpausing sound: id %d, muted\n", queue_id);
			return;
		}
		if (flags & SoundFlags_Fadeout)
		{
			SoundQueue[queue_id].flags &= ~SoundFlags_Fadeout;
			SoundQueue[queue_id].timer = 1;
		}
	}
	//PrintDebug("Unpausing sound: id %d, volume %d, dsound: %d\n", queue_id, conv_volume, volume_conv);
	Critical_SetSoundVolume(queue_id, volume_conv);
}

void InitDSound()
{
	// Volume stuff
	WriteJump((void*)0x423C20, lsndVolume); // Calculate sound volume (logarithmic)
	WriteCall((void*)0x4243D7, UnpauseSound_Fixed); // Same as above
	//WriteCall((void*)0x424BF2, Set3DPositionPCM_Hook); // Position override for 3D sound
}
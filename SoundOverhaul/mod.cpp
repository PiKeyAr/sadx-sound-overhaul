#include "stdafx.h"
#include "mod.h"
#include "SoundStructs.h"
#include "SoundStructsCustom.h"

// TODO: Rewrite with symbol names

// Config stuff

bool DelayedSoundsEnabled = true;
bool BoaBoaFix = true;
bool ClassicRingPan = true;
bool FadeOutEnabled = true;
bool ReplaceChaoSounds = true;
bool EnableChaoBGSounds = false;
int MovieVolumeSO = 100;
int MovieVolume_Real = 0;
int MusicVolumeBoost = 5100;
int VoiceVolumeBoost = 5100;

// Other stuff

bool SnowSoundFixed = false;
int MusicVolumeConv = 0;
int VoiceVolumeConv = 0;
int VolumeFixesApplied = false;
float explosiondelay = 0.2f;

// Converts linear (0-100) volume to DirectX volume used by WMP
int ConvertLinearToDirectX(int value, int max)
// Solution by JohnBolton, taken from https://www.gamedev.net/forums/topic/337397-sound-volume-question-directsound/?tab=comments#comment-3197281
{
	if (value <= 0)
	{
		return -10000;
	}
	else
	{
		return (int)floorf(2000.0f * log10f((float)(value) / (float)max));
	}
}

// Calculates volume for music and voices
int CalculateVolume_SADXStyle(int percent, int mode)
{
	// 0 for music, 1 for voices
	if (mode == 1) 
		return 100 * (97 * percent / 100 - 100);
	else 
		return 100 * (89 * percent / 100 - 100);
}

// Trampoline to override 3D sound distance
static Trampoline* Set3DMinMaxPCM_t = nullptr;
static bool __cdecl Set3DMinMaxPCM_r(int id, float dist_min, float dist_max)
{
	const auto original = TARGET_DYNAMIC(Set3DMinMaxPCM);
	float volume_scale = 1.0f;
	int volume_dist = 0;
	for (int i = 0; i < LengthOfArray(DistanceOverrideTable); i++)
	{
		if (DistanceOverrideTable[i].ID == SoundQueue[id].id && (!DistanceOverrideTable[i].volume || DistanceOverrideTable[i].volume == SoundQueue[id].volume))
		{
			//PrintDebug("Distance override: ID %d, min %f, max %f\n", id, DistanceOverrideTable[i].dist_min, DistanceOverrideTable[i].dist_max);
			return original(id, DistanceOverrideTable[i].dist_min, DistanceOverrideTable[i].dist_max);
		}
	}
	//PrintDebug("Distance: %d, %f, %f\n", id, dist_min, dist_max);
	return
		original(id, dist_min, dist_max);
}

extern "C"
{
	__declspec(dllexport) void __cdecl Init(const char* path, const HelperFunctions &helperFunctions)
	{
		if (GetModuleHandle(L"SoundOverhaul") != nullptr)
		{
			MessageBox(WindowHandle,
				L"Please enable only one version of Sound Overhaul at a time. The mod will not work.",
				L"Sound Overhaul error: Incompatible mods detected", MB_OK | MB_ICONERROR);
			return;
		}
		// Config stuff
		const IniFile* config = new IniFile(std::string(path) + "\\config.ini");
		BoaBoaFix = config->getBool("General", "BoaBoaFix", true);
		ClassicRingPan = config->getBool("General", "ClassicRingPan", true);
		FadeOutEnabled = config->getBool("General", "FadeOutEnabled", true);
		ReplaceChaoSounds = config->getBool("General", "ReplaceChaoSounds", true);
		EnableChaoBGSounds = config->getBool("General", "EnableChaoBGSounds", false);
		DelayedSoundsEnabled = config->getBool("General", "DelayedSounds", true);
		MusicVolumeBoost = config->getInt("Volume", "MusicVolumeBoost", 5100);
		VoiceVolumeBoost = config->getInt("Volume", "VoiceVolumeBoost", 5100);
		MovieVolumeSO = config->getInt("Volume", "MovieVolumeSO", 100);
		MusicVolumeConv = CalculateVolume_SADXStyle(GetPrivateProfileIntA("sonicDX", "bgmv", 89, ".\\sonicDX.ini"), 0) + MusicVolumeBoost;
		VoiceVolumeConv = CalculateVolume_SADXStyle(GetPrivateProfileIntA("sonicDX", "voicev", 100, ".\\sonicDX.ini"), 1) + VoiceVolumeBoost;
		MovieVolume_Real = ConvertLinearToDirectX(MovieVolumeSO, 100);
		WriteData((int**)0x005140DF, &MovieVolume_Real);
		// If lSndVolume is WriteJumped, assume BASS is enabled. If not, add the DSound hooks from the old version of the mod.
		if (lSndVolumeTest != 0xE9) // E9 is Jump
			InitDSound();
		// Chao stuff
		if (ReplaceChaoSounds)
			InitChaoSounds();
		if (EnableChaoBGSounds)
			InitChaoBGSounds();
		// Delayed sound effects
		if (DelayedSoundsEnabled)
			InitDelayedSounds();
		// Sound fadeout
		if (FadeOutEnabled)
			InitFadeout();
		// General changes
		InitSoundHooks();
		WriteData((void**)0x00410AE8, (void*)0x0088AC34); // Enable full HRTF mode
		WriteData((void**)0x00410CBC, (void*)0x0088AC34); // Enable full HRTF mode
		WriteData((int*)0x00423CD0, 44100); // Enable 44100hz sounds
		WriteData((int*)0x00410561, 44100); // Enable 44100hz sounds
		WriteData<1>((char*)0x0042508D, 0x11u); // Fix for sounds playing multiple times over
		if (ClassicRingPan) 
			WriteData((int*)0x00910290, 255); // Classic 100% stereo pan for ring collect sound
		// Distance override for 3D sounds
		Set3DMinMaxPCM_t = new Trampoline(0x004103B0, 0x004103B6, Set3DMinMaxPCM_r);
		// Apply fixes
		if (BoaBoaFix)
		{
			WriteCall((void*)0x79FCE4, PlaySound_XYZ_IgnoreDuplicates); // Boa-Boa
			WriteData<1>((char*)0x0079FCDA, 0xE0); // Volume for Boa sound
			WriteCall((void*)0x5E7316, PlaySound_XYZ_IgnoreDuplicates); // Lost World fire traps
			WriteData<1>((char*)0x5E730C, 0xE2); // Volume for fire traps
			WriteCall((void*)0x4EC573, PlaySound_XYZ_IgnoreDuplicates); // Ice Cap bomb toss
			WriteCall((void*)0x4EC3A1, PlaySound_XYZ_IgnoreDuplicates); // Ice Cap bomb explosion
		}
		// Various looping/timing/cutoff fixes
		WriteData<1>((char*)0x00621770, 2); // Prevent the Merry-Go-Round sound from being cut off by 3D sounds
		WriteCall((void*)0x004E26D9, PlaySound_StandardTimer_Delay); // Gamma destroying bridge in Windy Valley
		WriteData((float**)0x004E8B86, &explosiondelay); // Explosions after defeating E-series robots
		WriteCall((void*)0x0047F66D, PlaySound_Loop); // E102 jet sound loop
		WriteCall((void*)0x0047F732, PlaySound_Loop); // E102 jet sound loop
		WriteCall((void*)0x0047FCCA, PlaySound_Loop); // E102 jet sound loop (land)
		WriteData<1>((char*)0x0047F665, 0x01u); // Jet sound priority
		WriteData<1>((char*)0x0047F72A, 0x01u); // Jet sound priority
		WriteData<1>((char*)0x0047FCC2, 0x01u); // Jet sound priority (land)
		WriteData<1>((char*)0x004DF112, 0x79u); // Windy Valley wind path
		WriteData<1>((char*)0x0061B790, 0x60u); // Speed Highway fountain jump
		WriteData<1>((char*)0x005FC50A, 0x4Bu); // Sky Deck big cannon fire delay
		WriteData<1>((char*)0x005F3DB7, 0x4Bu); // Sky Deck big cannon fire delay
		WriteCall((void*)0x005F5FAF, AlarmFix); // Alarm in Sky Deck 2
		WriteCall((void*)0x0055BEC7, FreezerFix); // Chaos 6 freezer
		WriteData<1>((char*)0x005B7518, 0x30u); // Final Egg 3 fan timer
		WriteCall((void*)0x005568B1, StopSound_Fade); // Chaos 4 attack
		WriteCall((void*)0x00556845, PlaySound_StandardTimer); // Chaos 4 attack remove loop
		WriteData<1>((char*)0x00571AAF, 0x08); // Fix Egg Hornet sound loop
		// Volume fixes
		WriteData<1>((char*)0x0062B657, 0x78u); // SE_SHT_EMCCHARGE
		WriteData<1>((char*)0x0062B670, 0x78u); // SE_SHT_EMCFIRE
		WriteData<1>((char*)0x0062B670, 0x78u); // SE_SHT_EMCOPEN 1
		WriteData<1>((char*)0x0062B170, 0x78u); // SE_SHT_EMCOPEN 1
		WriteData<1>((char*)0x0062B760, 0x78u); // SE_SHT_EMCOPEN 1
		WriteData<1>((char*)0x0062AF71, 0x78u); // SE_SHT_EMCSTOP 1
		WriteData<1>((char*)0x0062B195, 0x78u); // SE_SHT_EMCSTOP 2
		WriteData<1>((char*)0x0062B7A2, 0x78u); // SE_SHT_EMCSTOP 3
		// Missing sound fixes
		WriteData<2>((void*)0x0053881F, 0x90u); // Enable ambient sound in MR Final Egg base
		WriteData<1>((char*)0x005EAE93, 0x50); // HebiGate sound fix
		WriteData<5>((char*)0x005EC440, 0x90u); // Remove SE_LW_POLE sound
		// Fixes for missing/wrong soundbanks
		SoundList_HotShelter[3].Filename = "FINAL_EGG_BANK04";
		WriteData((short*)0x00566CC9, (short)SE_ER_ITE); // E101 boss
		WriteData((short*)0x00567BA3, (short)SE_ER_ELEC); // E101 boss
		WriteData((short*)0x005674FC, (short)SE_ER_HOVER); // E101 boss
		WriteData((short*)0x00566E49, (short)SE_ER_FIRE); // E101 boss
		WriteData((short*)0x00568744, (short)SE_ER_MOVE); // E101 boss
		WriteData((short*)0x005681CA, (short)SE_ER_HIT); // E101 boss
		WriteData((short*)0x0056816B, (short)SE_ER_HIT); // E101 boss
		// Cutscene-related fixes
		WriteData<1>((char*)0x006AF86B, 0i8); // I forgot to put in the landing gear!
		WriteData<1>((char*)0x006CE07B, 0i8); // There's no landing gear in this mode!
		WriteData<1>((char*)0x006CA530, 0x34); // Goin' down! Aaaah! Look out below!
		WriteData<1>((char*)0x006CA54F, 0x78u); // Look out below timer
		WriteData<1>((char*)0x0066DC76, 111); // Gamma steals Froggy soundlist
		WriteData<1>((char*)0x006A40C8, 108); // Amy talking to Gamma (Amy's story) soundlist
		WriteData<1>((char*)0x00677F8B, 108); // Amy talking to Gamma (Gamma's story) soundlist
		WriteData<1>((char*)0x006CCC1B, 0x78u); // Chaos 0 uuuuuuoooooohhh!
		WriteCall((void*)0x006CC6E1, QueueBullet); // Chaos 0 
		WriteCall((void*)0x006CC782, QueueBullet); // Chaos 0 
		WriteCall((void*)0x006CC802, QueueBullet); // Chaos 0 
		WriteCall((void*)0x006CC885, QueueBullet); // Chaos 0 
		WriteCall((void*)0x006CC8A9, QueueBullet); // Chaos 0 
		WriteCall((void*)0x006CC8D0, QueueBullet); // Chaos 0 
		WriteCall((void*)0x006CC8F4, QueueBullet); // Chaos 0 
		WriteCall((void*)0x006CC91B, QueueBullet); // Chaos 0 
		WriteCall((void*)0x006CC93F, QueueBullet); // Chaos 0 
		WriteCall((void*)0x006CC966, QueueBullet); // Chaos 0 
		WriteCall((void*)0x006CC98A, QueueBullet); // Chaos 0 
		WriteCall((void*)0x006CC9B1, QueueBullet); // Chaos 0 
		WriteCall((void*)0x006CC9D5, QueueBullet); // Chaos 0 
		WriteCall((void*)0x006CC9FC, QueueBullet); // Chaos 0 
		WriteCall((void*)0x006CCA20, QueueBullet); // Chaos 0 
		WriteCall((void*)0x006CCA47, QueueBullet); // Chaos 0 
		// Debug
		//SkyboxObjects[0] = Load3DSoundTest; // 3D sound test in Hedgehog Hammer
	}

	__declspec(dllexport) void __cdecl OnFrame()
	{
		auto entity = EntityData1Ptrs[0];
		if (entity && !EV_MainThread_ptr)
		{
			if (CurrentCharacter == Characters_Gamma && EntityData1Ptrs[0]->Action != 31) 
				StopSoundID(SE_E_RDUSH);
		}
		//PrintDebug("Delayed sounds: %d\n", DelayedSoundCount);
		// Play delayed sounds one per frame
		if (DelayedSoundsEnabled)
			DelayedSoundsExec();
		// Fadeout
		if (FadeOutEnabled)
			SoundFadeExec();
		// Framerate fix for standard timer
		if (FramerateSetting >= 2 && TimerValue_PlaySound_Positional_StandardTimer != 120)
		{
			WriteData((int*)0x004250AE, 120);
		}
		if (FramerateSetting < 2 && TimerValue_PlaySound_Positional_StandardTimer == 120)
		{
			WriteData((int*)0x004250AE, 240);
		}
		// Ice Cap/Sand Hill thing
		if (CurrentLevel != 8 || CurrentAct != 2 || GameState == 3 || GameState == 4 || GameState == 7 || GameState == 21)
			SnowSoundFixed = false;
		if (entity != nullptr)
		{
			if (CurrentLevel == 8 && CurrentAct == 2)
			{
				if (SnowSoundFixed == false && entity->Status & Status_Ground && entity->Action == 62)
				{
					entity->Status &= ~Status_Ground;
					entity->Status &= ~Status_Unknown3;
					SnowSoundFixed = true;
				}
			}
		}
		if (!VolumeFixesApplied)
		{
			MusicVolume = MusicVolumeConv;
			VoiceVolume = VoiceVolumeConv; 
			VoiceVolumeBK = VoiceVolumeConv;
			VolumeFixesApplied = true;
		}
		//PrintDebug("VoiceVolume: %d \n", VoiceVolume);
		//PrintDebug("MusicVolume: %d \n", MusicVolume);
		//PrintDebug("MovieVolume: %d \n", MovieVolume_Real);
	}
	__declspec(dllexport) ModInfo SADXModInfo = { ModLoaderVer };
}
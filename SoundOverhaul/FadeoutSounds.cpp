#include "stdafx.h"
#include "FadeoutSounds.h"

// Sound fadeout system

void SoundFade_Apply(int queue_id)
{
	for (int i = 0; i < LengthOfArray(FadeoutTable); i++)
	{
		if (FadeoutTable[i].ID == SoundQueue[queue_id].id)
		{
			if (FadeoutTable[i].start >= SoundQueue[queue_id].timer)
			{
				SoundQueue[queue_id].volume -= FadeoutTable[i].speed;
			}
			if (SoundQueue[queue_id].volume <= -127)
				SoundQueue[queue_id].timer = 0;
		}
	}
	SoundQueue[queue_id].volume -= 4;
	if (SoundQueue[queue_id].volume <= -127)
		SoundQueue[queue_id].timer = 0;
	//PrintDebug("Def");
}

void SoundFadeExec()
{
	bool sounddone = false;
	for (int q = 0; q < 36; q++)
	{
		if (!EV_MainThread_ptr && SoundQueue[q].volume)
		{
			if (!(SoundQueue[q].flags & SoundFlags_VolumeManual) && !(SoundQueue[q].flags & SoundFlags_VolumeDist))
			{
				SoundQueue[q].flags |= SoundFlags_VolumeManual;
			}
		}
		if (SoundQueue[q].flags & SoundFlags_Fadeout)
		{
			SoundFade_Apply(q);
		}
	}
}

void PlaySpindash(int id, EntityData1* Data1, int a3, EntityData1* a4, int a5)
{
	if (CharObj2Ptrs[0])
	{
		if (CharObj2Ptrs[0]->Upgrades & Upgrades_LightShoes)
			PlaySound_FadeOut(767, Data1, 1, 0, 120);
		else
			PlaySound_Timer(767, Data1, 1, 0, 160);
	}
	else
		PlaySound_Timer(767, Data1, 1, 0, 160);
}

void InitFadeout()
{
	WriteCall((void*)0x496F33, PlaySpindash); // Spindash
	WriteCall((void*)0x4788A5, StopSound_Fade); // Knuckles' glide sound
	WriteCall((void*)0x46952F, StopSound_Fade); // Lost World mirror room
	WriteCall((void*)0x5E2ACD, StopSound_Fade); // Lost World mirror room 2
	WriteCall((void*)0x561DEA, StopSound_Fade); // Perfect Chaos emerging
	WriteCall((void*)0x561F14, StopSound_Fade); // Perfect Chaos sinking

	//WriteCall((void*)0x55EF45, StopSound_Fade); // Fade out Super Sonic's on water sound 1
	//WriteCall((void*)0x55EF8E, StopSound_Fade); // Fade out Super Sonic's on water sound 1
}